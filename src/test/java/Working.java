import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

import org.bouncycastle.openpgp.PGPException;
import org.c02e.jpgpj.Decryptor;
import org.c02e.jpgpj.Encryptor;
import org.c02e.jpgpj.HashingAlgorithm;
import org.c02e.jpgpj.Key;
import org.junit.Test;

public class Working {
    @Test
    public void testEncryptDecryptWithoutSigning() throws IOException, PGPException {
        // The data will be written to this property
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        // The data flows from a user, to the same user
        Encryptor encryptor = new Encryptor(
                new Key(new File(KeyBasedLargeFileProcessor.pathToFile("publickey")))
        );

        encryptor.withSigningAlgorithm(HashingAlgorithm.Unsigned);
        encryptor.setAsciiArmored(true);

        // Any data will be automatically encrypted
        OutputStream os = encryptor.prepareCiphertextOutputStream(baos, null, true);

        PrintWriter printWriter = new PrintWriter(
                new BufferedWriter(
                        new OutputStreamWriter(
                                os,
                                StandardCharsets.UTF_8.name())));

        // This is an example of super secret data to write
        String data = "Some very sensitive data";

        printWriter.print(data);
        printWriter.close();

        // At this point, the data is 'inside' the byte array property
        // Assert the text is encrypted
        if (baos.toString(StandardCharsets.UTF_8.name()).equals(data)) {
            throw new RuntimeException("baos not encrypted");
        }

        // Time to decrypt the data by using the private key of the user
        Decryptor decryptor = new Decryptor(
                new Key(new File(KeyBasedLargeFileProcessor.pathToFile("privatekey")), "test1")
        );

        decryptor.setVerificationRequired(false);

        ByteArrayInputStream inputStream = new ByteArrayInputStream(baos.toByteArray());
        ByteArrayOutputStream decrypted = new ByteArrayOutputStream();

        decryptor.decrypt(inputStream, decrypted);

        if (!decrypted.toString(StandardCharsets.UTF_8.name()).equals(data)) {
            throw new RuntimeException("Not successfully decrypted");
        }
    }
}
